/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejerecito.dtic.services;

import ec.mil.ejercito.dtic.entidades.TipoProducto;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author SEVILLA
 */
@Stateless
public class TipoProductoFacade extends AbstractFacade<TipoProducto> {

    @PersistenceContext(unitName = "dticPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoProductoFacade() {
        super(TipoProducto.class);
    }
    
   /**
    * Metodo para listar todos los datos de la tabla
    * @return 
    */ 
    public List<TipoProducto> buscarTodos(){ //public List<TipoProducto> (este es la lista que sera de tipo de nuestra table osea ListaProducto) ListaTipoProducto() (este ee el nombre de nuestro metodo)
        Query q = em.createQuery("Select t from TipoProducto t ORDER BY  t.descripcion ASC");
        return (List<TipoProducto>) q.getResultList();  // en la letra q. debemos coger single para un solo dato, y Result para todos los datos
    }
    
    
}
