/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejerecito.dtic.services;

import ec.mil.ejercito.dtic.entidades.Marisqueria;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author SEVILLA
 */
@Stateless
public class MarisqueriaFacade extends AbstractFacade<Marisqueria> {

    @PersistenceContext(unitName = "dticPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MarisqueriaFacade() {
        super(Marisqueria.class);
    }
    
}
