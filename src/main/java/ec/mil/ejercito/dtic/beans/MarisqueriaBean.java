/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.dtic.beans;

import ec.mil.ejercito.dtic.entidades.Marisqueria;
import ec.mil.ejercito.dtic.entidades.TipoMarisco;
import ec.mil.ejercito.dtic.util.Mensajes;
import ec.mil.ejerecito.dtic.services.MarisqueriaFacade;
import ec.mil.ejerecito.dtic.services.TipoMariscoFacade;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author SEVILLA
 */
@Named
@ViewScoped

public class MarisqueriaBean implements Serializable {

    /**
     * Listamiento de variables
     */
    private Marisqueria marisqueria;
    private boolean visible;
    private List<Marisqueria> listaMarisqueria;
    private List<TipoMarisco> listaTipoMarisco;
    private Date fechaIn;
    
    /**
     * Inyecciones 
     */
    @EJB
    private MarisqueriaFacade servicioMarisqueria;
    @EJB
    private TipoMariscoFacade servicioTipoMarisco;
    @Inject
    protected Mensajes mensajes;
    

    /**
     * Getters y Setters de variables
     */
    public Marisqueria getMarisqueria() {
        return marisqueria;
    }

    public void setMarisqueria(Marisqueria marisqueria) {
        this.marisqueria = marisqueria;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public List<Marisqueria> getListaMarisqueria() {
        return listaMarisqueria;
    }

    public void setListaMarisqueria(List<Marisqueria> listaMarisqueria) {
        this.listaMarisqueria = listaMarisqueria;
    }

    public List<TipoMarisco> getListaTipoMarisco() {
        return listaTipoMarisco;
    }

    public void setListaTipoMarisco(List<TipoMarisco> listaTipoMarisco) {
        this.listaTipoMarisco = listaTipoMarisco;
    }

    public MarisqueriaFacade getServicioMarisqueria() {
        return servicioMarisqueria;
    }

    public void setServicioMarisqueria(MarisqueriaFacade servicioMarisqueria) {
        this.servicioMarisqueria = servicioMarisqueria;
    }

    public TipoMariscoFacade getServicioTipoMarisco() {
        return servicioTipoMarisco;
    }

    public void setServicioTipoMarisco(TipoMariscoFacade servicioTipoMarisco) {
        this.servicioTipoMarisco = servicioTipoMarisco;
    }

    public Date getFechaIn() {
        return fechaIn;
    }

    public void setFechaIn(Date fechaIn) {
        this.fechaIn = fechaIn;
    }

    
    /**
     * Creamos el constructor
     */
    @PostConstruct
    public void init() {
        this.cargarMariscos();
        this.cargarTipoMarisco();
    }

    /**
     * Creamos metodos
     */
    public void cargarMariscos() {
        this.listaMarisqueria = servicioMarisqueria.findAll();
    }

    public void nuevoMarisco() {
        this.marisqueria = new Marisqueria();
        this.setVisible(true);
    }

    public void regresar() {
        this.setVisible(false);
    }

    public void cargarTipoMarisco() {
        this.listaTipoMarisco = this.servicioTipoMarisco.findAll();
    }

    public void guardar() {
        try {
            if (getMarisqueria().getIdMarisqueria() == null) {
                this.servicioMarisqueria.create(marisqueria);
                this.setVisible(false);
                mensajes.mostrarMensajes(1, "EXITO", "SI SE GUARDOOOOO");
            } else {
                this.servicioMarisqueria.edit(marisqueria);
                this.setVisible(false);
                mensajes.mostrarMensajes(1, "EXITO", "SI SE ACTUALIZOOO");
            }
            cargarMariscos();
        } catch (Exception e) {
            System.out.println("ERROR EN EL GUARDADO: " + e.getMessage());
        }
    }

    public void verItem(Marisqueria item) {
        this.setMarisqueria(item);
        this.setVisible(true);
    }

    public void eliminar(Marisqueria item) {
        try {
            this.servicioMarisqueria.remove(item);
            cargarMariscos();
            mensajes.mostrarMensajes(1, "EXITO", "DATO ELIMINADO");
        } catch (Exception e) {
            System.out.println("ERROR AL ELIMINAR DATO: "+ e.getMessage());
        }
    }

    
    
}
