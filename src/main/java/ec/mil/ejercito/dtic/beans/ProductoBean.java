/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.dtic.beans;

import ec.mil.ejercito.dtic.entidades.Producto;
import ec.mil.ejercito.dtic.entidades.TipoProducto;
import ec.mil.ejercito.dtic.util.Mensajes;
import ec.mil.ejerecito.dtic.services.ProductoFacade;
import ec.mil.ejerecito.dtic.services.TipoProductoFacade;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author SEVILLA
 */
@Named
@ViewScoped

public class ProductoBean implements Serializable {

    //Listamiento de variables
    private Producto producto;
    private List<Producto> listaProducto;
    private List<TipoProducto> listaTipoProducto;
    private Boolean visible;

    @EJB
    private ProductoFacade servicioProducto;

    @EJB
    private TipoProductoFacade servicioTipoProducto;

    // inyectamos instancia de una clase externa, en este caso MENSAJES
    @Inject
    protected Mensajes mensajes;

    //Insertamos Getter y Setter
    //<editor-fold defaultstate="collapsed" desc="GETTER SETTER">
    public ProductoFacade getServicioProducto() {
        return servicioProducto;
    }

    public void setServicioProducto(ProductoFacade servicioProducto) {
        this.servicioProducto = servicioProducto;
    }

    public List<Producto> getListaProducto() {
        return listaProducto;
    }

    public void setListaProducto(List<Producto> listaProducto) {
        this.listaProducto = listaProducto;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public TipoProductoFacade getServicioTipoProducto() {
        return servicioTipoProducto;
    }

    public void setServicioTipoProducto(TipoProductoFacade servicioTipoProducto) {
        this.servicioTipoProducto = servicioTipoProducto;
    }

    public List<TipoProducto> getListaTipoProducto() {
        return listaTipoProducto;
    }

    public void setListaTipoProducto(List<TipoProducto> listaTipoProducto) {
        this.listaTipoProducto = listaTipoProducto;
    }

    //<editor-fold>
    //Creamos el constructor
    @PostConstruct
    public void init() { //Siempre el mismo ="init" o "inicializar"

        //Aqui se debe listar todos los metodos que se realizó abajo del constructor que se ejecutan automaticamente como listamientos de datos
        this.cargarProductos();
        this.cargarTipoProducto();
    }

    //Creamos nuestros metodos
    public void cargarProductos() {
        this.listaProducto = servicioProducto.findAll();
    }

    public void nuevo() {
        this.producto = new Producto(); //instanciamos la entidad (de la tabla)
        this.setVisible(true);  // Al poner TRUE le decimos que esta variable boolean siempre esta ACTIVA
    }

    public void regresar() {
        this.setVisible(false);
    }

    // metodo guardar
    public void guardar() {
        try {
            if (getProducto().getIdProducto() == null) {
                this.servicioProducto.create(producto); //mandamos producto, que es la entidad que queremos ingresar datos( osea de mi tabla)
                this.setVisible(false);
                //usamos metodo del mensaje de la clase Mensajes para EXITO
                mensajes.mostrarMensajes(1, "EXITO", "Informacion NUEVA GUARDADA mijin");
            } else {
                this.servicioProducto.edit(producto);
                this.setVisible(false);
                //usamos metodo del mensaje de la clase Mensajes para EXITO
                mensajes.mostrarMensajes(1, "EXITO", "Informacion ACTUALIZADA mijin");
            }
            cargarProductos();
        } catch (Exception e) {
            System.out.println("ERROR EN EL GUARDADO: " + e.getMessage());
        }

    }

    /**
     * metodo para ver los datos o detalle del datos que escogemos
     *
     * @param item
     */
    public void verItem(Producto item) { // enviamos una variable de tipo Producto
        this.setProducto(item); // con el set estamos cogiendo todo lo que hay en la tabla producto y colocamos en la variable Item
        this.setVisible(true);
    }

    /**
     * METODO PARA ELIMINAR PRODUCTO
     *
     * @param item
     */
    public void eliminar(Producto item) {
        try {
            this.servicioProducto.remove(item);
            cargarProductos();
            mensajes.mostrarMensajes(1, "EXITO", "Registro ELIMINADO mijitrin");
        } catch (Exception e) {
            System.out.println("ERROR AL ELIMINAR REGISTRO: " + e.getMessage());
        }
    }

    /**
     * Metodo para cargar datos de la tabla TipoProducto
     */
    public void cargarTipoProducto() {
        this.listaTipoProducto = this.servicioTipoProducto.buscarTodos();
    }

}
