/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.dtic.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SEVILLA
 */
@Entity
@Table(name = "TIPO_PRODUCTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoProducto.findAll", query = "SELECT t FROM TipoProducto t"),
    @NamedQuery(name = "TipoProducto.findByIdTipoproducto", query = "SELECT t FROM TipoProducto t WHERE t.idTipoproducto = :idTipoproducto"),
    @NamedQuery(name = "TipoProducto.findByDescripcion", query = "SELECT t FROM TipoProducto t WHERE t.descripcion = :descripcion"),
    @NamedQuery(name = "TipoProducto.findByEstado", query = "SELECT t FROM TipoProducto t WHERE t.estado = :estado")})
public class TipoProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TIPOPRODUCTO")
    private BigDecimal idTipoproducto;
    @Size(max = 256)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Size(max = 32)
    @Column(name = "ESTADO")
    private String estado;
    @OneToMany(mappedBy = "Tipoproducto")
    private List<Producto> productoList;

    public TipoProducto() {
    }

    public TipoProducto(BigDecimal idTipoproducto) {
        this.idTipoproducto = idTipoproducto;
    }

    public BigDecimal getIdTipoproducto() {
        return idTipoproducto;
    }

    public void setIdTipoproducto(BigDecimal idTipoproducto) {
        this.idTipoproducto = idTipoproducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Producto> getProductoList() {
        return productoList;
    }

    public void setProductoList(List<Producto> productoList) {
        this.productoList = productoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoproducto != null ? idTipoproducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoProducto)) {
            return false;
        }
        TipoProducto other = (TipoProducto) object;
        if ((this.idTipoproducto == null && other.idTipoproducto != null) || (this.idTipoproducto != null && !this.idTipoproducto.equals(other.idTipoproducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.mil.ejercito.dtic.entidades.TipoProducto[ idTipoproducto=" + idTipoproducto + " ]";
    }
    
}
