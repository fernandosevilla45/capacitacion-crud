/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.dtic.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SEVILLA
 */
@Entity
@Table(name = "MARISQUERIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Marisqueria.findAll", query = "SELECT m FROM Marisqueria m"),
    @NamedQuery(name = "Marisqueria.findByIdMarisqueria", query = "SELECT m FROM Marisqueria m WHERE m.idMarisqueria = :idMarisqueria"),
    @NamedQuery(name = "Marisqueria.findByDescripcion", query = "SELECT m FROM Marisqueria m WHERE m.descripcion = :descripcion"),
    @NamedQuery(name = "Marisqueria.findByPrecio", query = "SELECT m FROM Marisqueria m WHERE m.precio = :precio"),
    @NamedQuery(name = "Marisqueria.findByEstado", query = "SELECT m FROM Marisqueria m WHERE m.estado = :estado"),
    @NamedQuery(name = "Marisqueria.findByFecha", query = "SELECT m FROM Marisqueria m WHERE m.fecha = :fecha")})
public class Marisqueria implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SEQ_MARISQUERIA", sequenceName = "SEQ_MARISQUERIA", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MARISQUERIA")
    @Column(name = "ID_MARISQUERIA")
    private BigDecimal idMarisqueria;
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Size(max = 10)
    @Column(name = "PRECIO")
    private String precio;
    @Column(name = "ESTADO")
    private Character estado;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "ID_TIPOMARISCO", referencedColumnName = "ID_TIPOMARISCO")
    @ManyToOne
    private TipoMarisco Tipomarisco;

    public Marisqueria() {
    }

    public Marisqueria(BigDecimal idMarisqueria) {
        this.idMarisqueria = idMarisqueria;
    }

    public BigDecimal getIdMarisqueria() {
        return idMarisqueria;
    }

    public void setIdMarisqueria(BigDecimal idMarisqueria) {
        this.idMarisqueria = idMarisqueria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public TipoMarisco getTipomarisco() {
        return Tipomarisco;
    }

    public void setTipomarisco(TipoMarisco Tipomarisco) {
        this.Tipomarisco = Tipomarisco;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMarisqueria != null ? idMarisqueria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Marisqueria)) {
            return false;
        }
        Marisqueria other = (Marisqueria) object;
        if ((this.idMarisqueria == null && other.idMarisqueria != null) || (this.idMarisqueria != null && !this.idMarisqueria.equals(other.idMarisqueria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.mil.ejercito.dtic.entidades.Marisqueria[ idMarisqueria=" + idMarisqueria + " ]";
    }

}
