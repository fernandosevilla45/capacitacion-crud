/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.dtic.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SEVILLA
 */
@Entity
@Table(name = "TIPO_MARISCO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoMarisco.findAll", query = "SELECT t FROM TipoMarisco t"),
    @NamedQuery(name = "TipoMarisco.findByIdTipomarisco", query = "SELECT t FROM TipoMarisco t WHERE t.idTipomarisco = :idTipomarisco"),
    @NamedQuery(name = "TipoMarisco.findByDescripcion", query = "SELECT t FROM TipoMarisco t WHERE t.descripcion = :descripcion"),
    @NamedQuery(name = "TipoMarisco.findByEstado", query = "SELECT t FROM TipoMarisco t WHERE t.estado = :estado")})
public class TipoMarisco implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TIPOMARISCO")
    private BigDecimal idTipomarisco;
    @Size(max = 256)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Size(max = 32)
    @Column(name = "ESTADO")
    private String estado;
    @OneToMany(mappedBy = "Tipomarisco")
    private List<Marisqueria> marisqueriaList;

    public TipoMarisco() {
    }

    public TipoMarisco(BigDecimal idTipomarisco) {
        this.idTipomarisco = idTipomarisco;
    }

    public BigDecimal getIdTipomarisco() {
        return idTipomarisco;
    }

    public void setIdTipomarisco(BigDecimal idTipomarisco) {
        this.idTipomarisco = idTipomarisco;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Marisqueria> getMarisqueriaList() {
        return marisqueriaList;
    }

    public void setMarisqueriaList(List<Marisqueria> marisqueriaList) {
        this.marisqueriaList = marisqueriaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipomarisco != null ? idTipomarisco.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoMarisco)) {
            return false;
        }
        TipoMarisco other = (TipoMarisco) object;
        if ((this.idTipomarisco == null && other.idTipomarisco != null) || (this.idTipomarisco != null && !this.idTipomarisco.equals(other.idTipomarisco))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ec.mil.ejercito.dtic.entidades.TipoMarisco[ idTipomarisco=" + idTipomarisco + " ]";
    }
    
}
